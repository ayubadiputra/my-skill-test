<?php

// Prevent from direct access
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'MST_Post_Type' ) ) :

/**
 * My_Skill_Skill Test register post type class.
 *
 * Register new custom post type and taxonomy for My_Skill_Skill Test.
 *
 * @class My_Skill_Skill Test
 * @package My_Skill_Skill Test
 * @since 1.0.0
 */
class MST_Post_Type {

    /**
     * Call register_post_type on init action.
     */
    public static function init() {
        add_action( 'init', array( __CLASS__, 'register_post_type' ) );
    }

    /**
     * Set skill test post type label and register it.
     */
    public static function register_post_type() {

        // Set post type label
        $labels = array(
            'name'                  => _x( 'My Skill Tests', 'My Skill Test general name', MST_DOMAIN ),
            'singular_name'         => _x( 'My Skill Test', 'My Skill Test singular name', MST_DOMAIN ),
            'menu_name'             => _x( 'My Skill Tests', 'Admin Menu text', MST_DOMAIN ),
            'name_admin_bar'        => _x( 'My Skill Test', 'Add New on Toolbar', MST_DOMAIN ),
            'add_new'               => __( 'Add New', MST_DOMAIN ),
            'add_new_item'          => __( 'Add New Skill Test', MST_DOMAIN ),
            'new_item'              => __( 'New Skill Test', MST_DOMAIN ),
            'edit_item'             => __( 'Edit Skill Test', MST_DOMAIN ),
            'view_item'             => __( 'View Skill Test', MST_DOMAIN ),
            'all_items'             => __( 'All Skill Tests', MST_DOMAIN ),
            'search_items'          => __( 'Search Skill Tests', MST_DOMAIN ),
            'parent_item_colon'     => __( 'Parent Skill Tests:', MST_DOMAIN ),
            'not_found'             => __( 'No skill tests found.', MST_DOMAIN ),
            'not_found_in_trash'    => __( 'No skill tests found in Trash.', MST_DOMAIN ),
            'featured_image'        => _x( 'Skill Test Cover Image', 'Overrides the “Featured Image” phrase for this skill test. Added in 4.3', MST_DOMAIN ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this skill test. Added in 4.3', MST_DOMAIN ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this skill test. Added in 4.3', MST_DOMAIN ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this skill test. Added in 4.3', MST_DOMAIN ),
            'archives'              => _x( 'Skill Test archives', 'The skill test archive label used in nav menus. Default “Post Archives”. Added in 4.4', MST_DOMAIN ),
            'insert_into_item'      => _x( 'Insert into skill test', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', MST_DOMAIN ),
            'uploaded_to_this_item' => _x( 'Uploaded to this skill test', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', MST_DOMAIN ),
            'filter_items_list'     => _x( 'Filter skill tests list', 'Screen reader text for the filter links heading on the skill test listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', MST_DOMAIN ),
            'items_list_navigation' => _x( 'Skill Tests list navigation', 'Screen reader text for the pagination heading on the skill test listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', MST_DOMAIN ),
            'items_list'            => _x( 'Skill Tests list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', MST_DOMAIN ),
        );

        // Get slug from options
        $slug = mst_get_option( 'mst_skill_test_slug', 'test-skill' );

        // Post type arguments
        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'show_in_admin_bar'  => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => $slug ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => 21,
            'menu_icon'          => 'dashicons-analytics',
            'can_export'         => true,
            'supports'           => array( 'title', 'editor', 'comments', 'page-attributes', 'thumbnail', 'post-formats', 'author' ),
        );

        register_post_type( 'mst_skill_test', $args );

    }

}

endif;

MST_Post_Type::init();