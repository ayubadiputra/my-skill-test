<?php

/**
 * Skill details meta box.
 *
 * Display all additional metabox for Skill post type.
 *
 * @package My_Skill_Test
 * @since 1.0.0
 */

global $post; ?>

<table class="form-table">
    <!-- Maximum Test Time -->
    <tr>
        <th scope="row">
            <label for="max_test_time"><?php _e( 'Maximum test time', MST_DOMAIN ); ?></label>
        </th>
        <td>
            <?php
                $max_time = get_post_meta( $post->ID, 'mst_max_test_time', true );
                $max_time = ( ! empty( $max_time ) ) ? $max_time : '' ;
            ?>
            <input name="mst_skill_test_details[max_test_time]" type="number" max="300" id="max_test_time" value="<?php echo esc_attr( $max_time ); ?>" class="regular-text">
            <span class="sp-note"><?php _e( 'minutes, max: 300 minutes', MST_DOMAIN ); ?></span>
        </td>
    </tr>
    <!-- Finish in -->
    <?php
        $taken_time = get_post_meta( $post->ID, 'mst_test_taken_time_display', true );
        $taken_time = ( ! empty( $taken_time ) ) ? $taken_time : '' ;
        if ( ! empty( $taken_time ) ) :
    ?>
    <tr>
        <th scope="row">
            <label for="test_time_taken"><?php _e( 'Finish in', MST_DOMAIN ); ?></label>
        </th>
        <td>
            <p><?php echo $taken_time; ?></p>
        </td>
    </tr>
    <?php endif; ?>
    <!-- Minimum Point -->
    <tr>
        <th scope="row">
            <label for="min_test_point"><?php _e( 'Minimum test point', MST_DOMAIN ); ?></label>
        </th>
        <td>
            <?php
                $min_point = get_post_meta( $post->ID, 'mst_min_test_point', true );
                $min_point = ( ! empty( $min_point ) ) ? $min_point : '' ;
            ?>
            <input name="mst_skill_test_details[min_test_point]" type="number" max="100" id="min_test_point" value="<?php echo esc_attr( $min_point ); ?>" class="regular-text">
            <span class="sp-note"><?php _e( 'points, max: 100', MST_DOMAIN ); ?></span>
        </td>
    </tr>
    <!-- Status -->
    <tr>
        <th scope="row">
            <label for="test_status"><?php _e( 'Status', MST_DOMAIN ); ?></label>
        </th>
        <td>
            <p>
            <?php
                $status = get_post_meta( $post->ID, 'mst_test_status', true );
                $status = ( ! empty( $status ) ) ? $status : 'Not yet' ;

                $data = apply_filters( 'mst_status_data', array(
                    'not-yet' => 'Not yet',
                    'pass'    => 'Pass',
                    'fail'    => 'Fail'
                ));
            ?>
                <select class="hide" id="test_status" name="mst_skill_test_details[test_status]">
                    <?php
                        foreach ( $data as $key => $value ) {
                            $selected = '';
                            if ( $value == $status ) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $value . '" ' . $selected . '>' . $value . '</option>';
                        }
                    ?>
                </select>
            </p>
            <!-- <p id="note-status"><?php echo $status; ?></p>
            <p>
                <input type="checkbox" id="change-status" /> Change status
            </p> -->
        </td>
    </tr>
    <!-- Invitation -->
    <tr>
        <th scope="row">
            <label for="test_invitation"><?php _e( 'Test invitation', MST_DOMAIN ); ?></label>
        </th>
        <td>
            <?php
                $test_invitation = get_post_meta( $post->ID, 'mst_test_invitation', true );
                $test_invitation = ( ! empty( $test_invitation ) ) ? $test_invitation : date( 'F d, Y H:i' ) ;
                $test_invitation = date( 'F d, Y H:i', strtotime( $test_invitation ) );
            ?>
            <input id="test_invitation" name="mst_skill_test_details[test_invitation]" type="text" max="100" id="test_invitation" value="<?php echo esc_attr( $test_invitation ); ?>" class="regular-text">
        </td>
    </tr>
    <!-- Start -->
    <tr>
        <th scope="row">
            <label for="test_start"><?php _e( 'Test start', MST_DOMAIN ); ?></label>
        </th>
        <td>
            <?php
                $test_start = get_post_meta( $post->ID, 'mst_test_start', true );
                $test_start = ( ! empty( $test_start ) ) ? $test_start : date( 'F d, Y H:i' ) ;
                $test_start = date( 'F d, Y H:i', strtotime( $test_start ) );
            ?>
            <input id="test_start" name="mst_skill_test_details[test_start]" type="text" max="100" id="test_start" value="<?php echo esc_attr( $test_start ); ?>" class="regular-text">
        </td>
    </tr>
    <!-- Complete -->
    <tr>
        <th scope="row">
            <label for="test_complete"><?php _e( 'Test complete', MST_DOMAIN ); ?></label>
        </th>
        <td>
            <?php
                $test_complete = get_post_meta( $post->ID, 'mst_test_complete', true );
                $test_complete = ( ! empty( $test_complete ) ) ? $test_complete : date( 'F d, Y H:i' ) ;
                $test_complete = date( 'F d, Y H:i', strtotime( $test_complete ) );
            ?>
            <input id="test_complete" name="mst_skill_test_details[test_complete]" type="text" max="100" id="test_complete" value="<?php echo esc_attr( $test_complete ); ?>" class="regular-text">
        </td>
    </tr>
</table>