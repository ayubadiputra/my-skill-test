<?php

/**
 * Skill taken meta box.
 *
 * Display all additional metabox for Skill post type.
 *
 * @package My_Skill_Test
 * @since 1.0.0
 */

global $post; ?>

<div id="postcustomstuff">
    <?php wp_nonce_field( 'mst_skill_test_details_action', 'mst_skill_test_details_field' ); ?>
    <table id="" class="mst-skill-taken-table">
        <thead>
            <tr>
                <th>
                    <label for="skill_name">
                        <?php _e( 'Skill name (max. 50 words)', MST_DOMAIN ); ?>
                    </label>
                </th>
                <th>
                    <label for="skill_value">
                        <?php _e( 'Skill value (0-100)', MST_DOMAIN ); ?>
                    </label>
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $skill_taken = get_post_meta( $post->ID, 'mst_test_taken_skill', true );
                if ( ! empty( $skill_taken ) ) :
                    foreach ( $skill_taken as $key => $value ) :
            ?>
            <tr class="skill_data">
                <td id="skill_name">
                    <?php $skill_name = $value['skill_name']; ?>
                    <input type="text" id="skill_name" class="skill_name" name="mst_skill_test_details[skill_name][]" maxlength="50" value="<?php echo $skill_name; ?>">
                </td>
                <td id="skill_value">
                    <?php $skill_value = $value['skill_value']; ?>
                    <input type="number" id="skill_value" class="skill_value" name="mst_skill_test_details[skill_value][]" maxlength="3" value="<?php echo $skill_value; ?>">
                </td>
                <td class="skill_remove" style="width: 30px;">
                    <div class="remove-container">
                        <a class="button remove-row">
                            <span class="dashicons-before dashicons-no"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endforeach; else : ?>
            <tr class="skill_data">
                <td id="skill_name">
                    <input type="text" id="skill_name" class="skill_name" name="mst_skill_test_details[skill_name][]" maxlength="50" value="">
                </td>
                <td id="skill_value">
                    <input type="number" id="skill_value" class="skill_value" name="mst_skill_test_details[skill_value][]" maxlength="3" value="">
                </td>
                <td class="skill_remove" style="width: 30px;">
                    <div class="remove-container">
                        <a class="button remove-row">
                            <span class="dashicons-before dashicons-no"></span>
                        </a>
                    </div>
                </td>
            </tr>
            <?php endif; ?>

            <tr>
                <td colspan="2">
                    <div class="add">
                        <a id="add-new-row" class="button add-new-row">Add Skill</a>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- <p>Custom fields can be used to add extra metadata to a post that you can <a href="https://codex.wordpress.org/Using_Custom_Fields">use in your theme</a>.</p> -->
</div>