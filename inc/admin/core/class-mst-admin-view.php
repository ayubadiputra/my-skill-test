<?php

// Prevent from direct access
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'MST_Admin_View' ) ) :

/**
 * Render view in admin pages.
 *
 * Called by controller to render view page.
 *
 * @class MST_Admin_View
 * @package My_Skill_Test
 * @since 1.0.0
 */
class MST_Admin_View {

    /**
     * Render admin page by calling the filename and pass arguments.
     *
     * @param  string $filename Filename without extension
     * @param  array  $args     Pass arguments, must be in array
     * @return                  Only return empty if the file is not exist
     */
    public static function render( $post_object = false, $args = array() ) {

        // Convert arguments into variable
        if ( ! empty( $args['args'] ) ) {
            extract( $args['args'] );
        }

        if ( ! isset( $filename ) ) {
            return;
        }

        // Set file path
        $view_path = apply_filters(
            'mst_admin_view_path',
            trailingslashit( MST_DIR_ADMIN . 'views' )
        );

        // Check page should be exist
        $file = $view_path . $filename . '.php';
        if ( false === file_exists( $file ) ) {
            return;
        }

        require_once( $file );

    }

}

endif;