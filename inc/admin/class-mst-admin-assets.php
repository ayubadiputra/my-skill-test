<?php

// Prevent from direct access
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'MST_Admin_Assets' ) ) :

/**
 * My_Skill_Test frontend assets.
 *
 * Enqueue main frontend asset files for My_Skill_Test.
 *
 * @class MST_Admin_Assets
 * @package My_Skill_Test
 * @since 1.0.0
 */
class MST_Admin_Assets {

    /**
     * Call register_post_type on init action.
     */
    public static function init() {
        add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_assets' ) );
    }

    /**
     * Set test post type label and register it.
     */
    public static function enqueue_assets() {

        // Stylesheet
        wp_register_style( 'mst-styles', MST_URI_ASSETS . 'css/my-skill-test-admin.css', '1.0.0' );
        wp_enqueue_style( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
        wp_enqueue_style( 'jquery-time-picker', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css' );

        // Scripts
        wp_enqueue_script( 'jquery-ui-datetimepicker', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.js', array( 'jquery-ui-core' ,'jquery-ui-datepicker', 'jquery-ui-slider' ), '1.6.3' );
        wp_enqueue_script( 'mst-scripts', MST_URI_ASSETS . 'js/my-skill-test-admin.js', array( 'jquery-ui-core' ,'jquery-ui-datepicker', 'jquery-ui-slider', 'jquery-ui-datetimepicker' ), '1.0.0' );

        wp_enqueue_style( 'mst-styles' );
        wp_enqueue_style( 'jquery-ui-datetimepicker' );
        wp_enqueue_style( 'jquery-ui' );

    }

}

endif;

MST_Admin_Assets::init();