<?php

// Prevent from direct access
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'MST_Meta_Boxes' ) ) :

/**
 * My_Skill_Test frontend assets.
 *
 * Add meta box in admin page.
 *
 * @class MST_Meta_Boxes
 * @package My_Skill_Test
 * @since 1.0.0
 */
class MST_Meta_Boxes {

    /**
     * Call multiple add meta box on init action.
     */
    public static function init() {
        add_action( 'add_meta_boxes', array( __CLASS__, 'skill_test_metabox' ) );
        add_action( 'save_post', array( __CLASS__, 'save_skill_test' ), 1, 2);
        add_filter( 'manage_mst_skill_test_posts_columns', array( __CLASS__, 'set_custom_edit_columns' ) );
        add_action( 'manage_mst_skill_test_posts_custom_column' , array( __CLASS__, 'custom_columns' ), 10, 2 );
    }

    /**
     * Add mst meta box for skill taken and test details.
     */
    public static function skill_test_metabox() {
        add_meta_box( 'mst-skill-taken-meta', 'Skill Taken', array( 'MST_Admin_View', 'render' ), 'mst_skill_test', 'normal', 'high', array( 'filename' => 'skill-taken' ) );
        add_meta_box( 'mst-skill-test-meta', 'Test Skill Details', array( 'MST_Admin_View', 'render' ), 'mst_skill_test', 'normal', 'high', array( 'filename' => 'details' ) );
    }

    /**
     * Handle save post meta for mst skill taken and details only.
     * @param  int    $post_id Current post ID
     * @param  object $post    Current post in object
     * @return mixed           Return for error found
     */
    public static function save_skill_test( $post_id, $post ) {

        // Check nonce field
        if ( ! isset( $_POST['mst_skill_test_details_field'] ) ) {
            return;
        }

        // Check current post type
        if ( $post->post_type != 'mst_skill_test' ){
            return;
        }

        // Check if page is new post
        global $pagenow;
        if ( in_array( $pagenow, array( 'post-new.php' ) ) ) {
            return;
        }

        mst_access_check_save( $post, 'mst_skill_test_details' );

        if ( ! isset( $_POST['mst_skill_test_details'] ) ) {
            return;
        }

        // Integer needed
        $int_needed = array(
            'max_test_time', 'min_test_point'
        );

        // Datetime format
        $datetime_needed = array(
            'test_invitation', 'test_start', 'test_complete'
        );

        // Skill needed
        $skill_needed = array(
            'skill_name', 'skill_value'
        );

        $data = (array) $_POST['mst_skill_test_details']; $lapse = array();
        foreach ( $data as $key => $value ) {

            if ( in_array( $key, $skill_needed ) ) {
                continue;
            }

            $value = sanitize_text_field( $value );

            if ( in_array( $key, $int_needed ) ) {

                // Check integer
                $value = intval( $value );
                if ( ! $value ) {
                    continue;
                }

                $top_limit = ( $key == 'max_test_time' ) ? 300 : 100;
                if ( $value < 0 || $value > $top_limit ) {
                    continue;
                }

            } elseif ( in_array( $key, $datetime_needed ) ) {
                // Change date format
                $value         = date( 'Y-m-d H:i:s', strtotime( $value ) );
                $lapse[ $key ] = $value;
            }

            update_post_meta( $post->ID, 'mst_' . $key, $value );

        }

        // Skills taken
        $data_skill = array(); $data_value_total = 0; $data_value_average = 0; $i = 0;
        if ( ! empty( $data['skill_name'] ) && ! empty( $data['skill_value'] ) ) {
            foreach ( $data['skill_name'] as $skill_key => $skill_value ) {
                $data_skill[]  = array(
                    'skill_name'  => $data['skill_name'][ $skill_key ],
                    'skill_value' => $data['skill_value'][ $skill_key ],
                );
                $data_value_total += $data['skill_value'][ $skill_key ];
                $i++;
            }

            $data_value_average = round( $data_value_total / $i );

        }

        update_post_meta( $post->ID, 'mst_test_taken_skill', $data_skill );
        update_post_meta( $post->ID, 'mst_test_taken_skill_value', $data_value_average );

        // Test taken time
        if ( ! empty( $lapse ) ) {
            // Check datetime format
            $test_taken_time = mst_get_lapse_of_time( $lapse['test_start'], $lapse['test_complete'], true );
            update_post_meta( $post->ID, 'mst_test_taken_time_display', $test_taken_time['display'] );
            update_post_meta( $post->ID, 'mst_test_taken_time', $test_taken_time['save'] );
        }

    }

    public static function set_custom_edit_columns( $columns ) {
        $columns['breakdown'] = __( 'Breakdown', MST_DOMAIN );
        return $columns;
    }

    public static function custom_columns( $column, $post_id ) {
        switch ( $column ) {
            case 'breakdown':
                echo '[mst-breakdown mst-id="' . $post_id . '"]';
                break;
        }
    }

}

endif;

MST_Meta_Boxes::init();