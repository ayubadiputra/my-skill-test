<?php

/**
 * Collection of helper functions
 *
 * @package My_Skill_Test
 */

if ( ! function_exists( 'mst_change_link' ) ) {

    /**
     * Generate change link if user is admin.
     */
    function mst_change_link() {

        if ( is_user_logged_in() ) {
            $current_user = wp_get_current_user();
            if (user_can( $current_user, 'administrator' )) {
                edit_post_link( __( 'Change', MST_DOMAIN ) );
            }
        }

    }

}

if ( ! function_exists( 'mst_minute_to_hour' ) ) {

    /**
     * Convert minutes to hours
     * @param  integer $minutes Minutes in number
     * @return string           New format in h min
     */
    function mst_minute_to_hour( $minutes ) {

        if ( $minutes >= 60 ) {
            $hour = floor( $minutes / 60 );
            $min  = $minutes % 60;
        } else {
            return $minutes . 'min';
        }

        return $hour . 'h ' . $min . 'min';

    }

}

if ( ! function_exists( 'mst_get_lapse_of_time' ) ) {

    /**
     * Get lapse time between current time and comment time
     *
     * @param  string  $start  Start datetime
     * @param  string  $end    End datetime
     * @param  boolean $format Datetime text format
     */
    function mst_get_lapse_of_time( $start = '', $end = '', $formal_format = false ) {

        $start_time  = date_create( date( 'Y-m-d H:i:s', strtotime( $start ) ) );
        $end_time    = date_create( date( 'Y-m-d H:i:s', strtotime( $end ) ) );
        $time_format = array(
            'y' => __( ' year', MST_DOMAIN ),
            'm' => __( ' month', MST_DOMAIN ),
            'd' => __( ' day', MST_DOMAIN ),
            'h' => __( 'h', MST_DOMAIN ),
            'i' => __( 'min', MST_DOMAIN )
        );

        $diff   = date_diff( $start_time, $end_time );
        $output = array(
            'display' => '',
            'save'    => $diff->format( '%y-%m-%d %h:%i:%s' )
        );
        foreach ( $diff as $key => $value ) {
            if ( $value != 0 ) {
                if ( $key == 's' ) {
                    echo  __( 'Just now', MST_DOMAIN );
                    return false;
                }
                $format = $key; $many = '';
                if ( $formal_format ) {
                    $format = $time_format[$key];
                    $many   = 's';
                }
                if ( $key == 'h' || $key == 'i' ) {
                    $many = '';
                }
                $format = ( $value > 1 ) ? $format . $many : $format;
                $format = ( $format == 'i' ) ? 'm' : $format ;
                $output['display'] .= sprintf( __( '%1$s%2$s ', MST_DOMAIN ), $value, $format );
            }
        }

        return $output;

    }

}

if ( ! function_exists( 'is_mst_type' ) ) {

    /**
     * is_mst_type - Returns true when viewing a single mst_type.
     * @return bool
     */
    function is_mst_type() {
        return is_singular( array( 'mst_skill_test' ) );
    }
}

if ( ! function_exists( 'mst_generated_class' ) ) {

    /**
     * Create generated class based on page.
     *
     * @since 1.0.0
     *
     * @param  $additional  array|string Additional class
     */
    function mst_generated_class( $additional = false ) {

        $classes = 'mst ';

        // Generate class based on page
        if ( is_mst_type() ) {
            $classes .= 'mst-single ';
        }

        // Check key is string and not false
        if ( ! empty( $additional ) ) {
            $additional = (array) $additional;
            foreach ( $additional as $key => $value ) {
                $classes .= $value . ' ';
            }
        }

        echo 'class="' . $classes . '"';

    }
}

/**
 * Collection of helper functions
 *
 * @package My_Skill_Test
 */

if ( ! function_exists( 'mst_get_option' ) ) {

    /**
     * Return key value of MST options
     *
     * @since 1.0.0
     *
     * @param  string $key     Key of the option
     * @param  mixed  $default Default value if key not exist
     * @return mixed           Value of the key, false if key empty
     */
    function mst_get_option( $key = false, $default = false ) {

        // Check key is string and not false
        if ( (string) $key !== $key ) {
            return false;
        }

        // Get mst options
        $options = get_option( 'mst_options', false );

        // If the key is 'all', return all option values
        if ( $key == 'all' ) {
            return $options;
        }

        // Check key if not exist, return default
        if ( ! isset( $options[ $key ] ) ) {
            return $default;
        }

        return $options[ $key ];

    }
}

if ( ! function_exists( 'mst_access_check' ) ) {

    /**
     * Check current user access
     *
     * @since 1.0.0
     *
     * @param  boolean $type Type of action
     */
    function mst_access_check( $type = false ) {

        // Check current user can activate the plugin
        if ( ! current_user_can( 'activate_plugins' ) ) {
            return;
        }

        // Get plugin name
        $plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';

        // Choose the action based on type
        switch ( $type ) {

            case 'activation':
                $action = 'activate-plugin_' . $plugin;
                break;

            case 'deactivation':
                $action = 'deactivate-plugin_' . $plugin;
                break;

            case 'uninstallation':
                $action = 'bulk-plugins';
                break;

            default:
                $action = false;
                break;

        }

        // Check if page request is referred from other admin page
        if ( $action != false ) {
            check_admin_referer( $action );
        }

    }

    /**
     * Check current user access for save data
     *
     * @since 1.0.0
     *
     * @param  boolean $type Type of action
     */
    function mst_access_check_save( $post, $action ) {

        // Check current user can edit current post
        if ( ! current_user_can( 'edit_post', $post->ID ) ) {
            return $post->ID;
        }

        // Check nonce
        if ( ! check_admin_referer( $action . '_action', $action . '_field' ) ) {
            return $post->ID;
        }

    }
}
