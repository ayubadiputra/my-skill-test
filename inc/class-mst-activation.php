<?php

// Prevent from direct access
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'MST_Activation' ) ) :

/**
 * Activation class
 *
 * Handle activation and deactivtion hooks. Deleting custom options and cache
 * on uninstallation process.
 *
 * @class MST_Activation
 * @package My_Skill_Test
 * @since 1.0.0
 */
class MST_Activation {

    /**
     * Create new option called mst_options during installation process if not exist.
     * Set mst_active value to true then.
     */
    public static function activation() {

        // Check current user access
        mst_access_check( 'activation' );

        // Get and set active option
        $mst_options = get_option( 'mst_options', false );

        // If options exist
        if ( $mst_options != false ) {

            // Update active option is true
            $mst_options['mst_active'] = true;
            update_option( 'mst_options', $mst_options );
            return;

        }

        // Set mst_options
        $mst_options = array(
            'mst_active'          => true,
            'mst_skill_test_slug' => 'test-skill'
        );

        // Add plugin option
        add_option( 'mst_options', $mst_options );

    }

    /**
     * Set mst_active value to false during deactivation process
     */
    public static function deactivation() {

        // Check current user access
        mst_access_check( 'deactivation' );

        // Update active option is false
        $mst_options['mst_active'] = false;
        update_option( 'mst_options', $mst_options );

    }

    /**
     * Delete mst_options during uninstallation process
     */
    public static function uninstall() {

        // Check current user access
        mst_access_check( 'uninstallation' );

        // Delete all My_Skill_Test options
        delete_option( 'mst_options' );

    }

}

endif;