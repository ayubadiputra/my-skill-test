<?php

// Prevent from direct access
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'MST_Assets' ) ) :

/**
 * My_Skill_Test frontend assets.
 *
 * Enqueue main frontend asset files for My_Skill_Test.
 *
 * @class MST_Assets
 * @package My_Skill_Test
 * @since 1.0.0
 */
class MST_Assets {

    /**
     * Call register_post_type on init action.
     */
    public static function init() {
        add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_assets' ) );
    }

    /**
     * Set test post type label and register it.
     */
    public static function enqueue_assets() {

        // Stylesheet
        wp_register_style( 'mst-styles', MST_URI_ASSETS . 'css/my-skill-test.css', '1.0.0' );
        wp_register_style( 'mst-chartist', MST_URI_ASSETS . 'third-party/chartist/chartist.css', '0.10.0' );

        // Scripts
        wp_register_script( 'mst-chartist', MST_URI_ASSETS . 'third-party/chartist/chartist.min.js', array(), '0.10.0' );
        wp_register_script( 'mst-scripts', MST_URI_ASSETS . 'js/my-skill-test.js', array( 'jquery' ), '0.10.0' );

        // Localize the script with new data
        $translation_array = array(
            'ajax_url' => admin_url( 'admin-ajax.php' )
        );
        wp_localize_script( 'mst-scripts', 'mst_ajax', $translation_array );

        wp_enqueue_style( 'mst-styles' );
        wp_enqueue_style( 'mst-chartist' );
        wp_enqueue_script( 'mst-chartist' );
        wp_enqueue_script( 'mst-scripts' );

    }

}

endif;

MST_Assets::init();