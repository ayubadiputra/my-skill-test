<?php

/**
 * Collection of templates functions
 *
 * @package My_Skill_Test
 * @since 1.0.0
 */

if ( ! function_exists( 'mst_page_template' ) ) {

    /**
     * Return current template file
     * @param  string $template Current template file.
     * @return string           Custom/new template file.
     */
    function mst_page_template( $template ) {

        if ( is_singular( 'mst_skill_test' ) ) {
            $template = MST_DIR_TEMPLATES . 'single-mst_skill_test.php';
        }

        return $template;
    }

}
add_filter( 'template_include', 'mst_page_template', 99 );


if ( ! function_exists( 'mst_get_template_part' ) ) {

    /**
     * Return current template file
     * @param  string $template Current template file.
     * @return string           Custom/new template file.
     */
    function mst_get_template_part( $template, $param = array() ) {

        // Pass param data
        set_query_var( $param['key'], $param['data'] );

        $template = MST_DIR_TEMPLATES . 'parts/' . $template . '.php';
        if ( file_exists( $template ) ) {
            load_template( $template );
        }

    }

}
add_filter( 'template_include', 'mst_page_template', 99 );