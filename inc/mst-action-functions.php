<?php

/**
 * Collection of action hooks
 *
 * @package My_Skill_Test
 */

if ( ! function_exists( 'mst_override_index_posts' ) ) {

    /**
     * Override post post type with mst_skill_test in index.
     * @param  object $query Current main query
     */
    function mst_override_index_posts( $query ) {
        if ( $query->is_home() && $query->is_main_query() ) {
            $query->set( 'post_type', 'mst_skill_test' );
        }
    }

}
add_action( 'pre_get_posts', 'mst_override_index_posts' );

if ( ! function_exists( 'mst_display_breakdown' ) ) {

    /**
     * Display breakdown section in single pages.
     * @param  array $atts Shortcode pass variable
     */
    function mst_display_breakdown( $atts ) {

        // Attribute data
        $data = extract( shortcode_atts( array(
            'key'   => 'mst_breakdown'
        ), $atts ) );

        if ( ! empty( $atts['mst-id'] ) ) {
            $post_id = $atts['mst-id'];
        } else {
            $post_id = get_the_ID();
        }

        // Set data for display
        $data = array(
            'key'   => 'mst_breakdown',
            'data'  => array(
                'id' => $post_id
            )
        );

        mst_get_template_part( 'single-breakdown', $data );

    }

}
add_shortcode( 'mst-breakdown', 'mst_display_breakdown' );

if ( ! function_exists( 'mst_display_results' ) ) {

    /**
     * Display 'display' section in single mst.
     */
    function mst_display_results() {

        // Set max time
        $max_time = get_post_meta( get_the_ID(), 'mst_max_test_time', true );
        $max_time = mst_minute_to_hour( $max_time );

        // Set data for display
        $data = array(
            'key'   => 'mst_result',
            'data'  => array(
                'status'     => get_post_meta( get_the_ID(), 'mst_test_status', true ),
                'time_taken' => get_post_meta( get_the_ID(), 'mst_test_taken_time_display', true ),
                'max_time'   => $max_time,
                'value'      => get_post_meta( get_the_ID(), 'mst_test_taken_skill_value', true )
            )
        );

        mst_get_template_part( 'single-results', $data );
    }

}
add_action( 'mst_display_results', 'mst_display_results' );

if ( ! function_exists( 'mst_display_history' ) ) {

    /**
     * Display history section in single mst.
     */
    function mst_display_history() {

        // Set data for display
        $data = array(
            'key'   => 'mst_history',
            'data'  => array(
                'invitation' => date(
                    'd M', strtotime( get_post_meta( get_the_ID(), 'mst_test_invitation', true ) )
                ),
                'started'    => date(
                    'd M', strtotime( get_post_meta( get_the_ID(), 'mst_test_start', true ) )
                ),
                'completed'  => date(
                    'd M', strtotime( get_post_meta( get_the_ID(), 'mst_test_complete', true ) )
                ),
            )
        );

        mst_get_template_part( 'single-history', $data );
    }

}
add_action( 'mst_display_history', 'mst_display_history' );

if ( ! function_exists( 'mst_display_breakdown_item' ) ) {

    /**
     * Display breakdown skill items section in single mst.
     */
    function mst_display_breakdown_item() {

        $post_id = $_POST['mst_id'];

        // Get skill taken value
        $data = get_post_meta( $post_id, 'mst_test_taken_skill', true );
        if ( ! empty( $data ) ) :
            foreach ( $data as $key => $value ) : ?>

        <div class="mst-breakdown-item">
            <div class="skill">
                <div class="skill-name"><?php echo esc_html( $value['skill_name'] ); ?></div>
            </div>
            <div class="progress-area">
                <div class="progress">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo esc_html( $value['skill_value'] ); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo esc_html( $value['skill_value'] ); ?>%">
                        <span class="sr-only"><?php echo esc_html( $value['skill_value'] ); ?>% Complete (info)</span>
                    </div>
                </div>
            </div>
            <div class="precentage">
                <?php echo esc_html( $value['skill_value'] ); ?>%
            </div>
        </div>

<?php       endforeach;
        else :
            echo '<div class="mst-breakdown-item"><p>' . _e( 'No skill taken', MST_DOMAIN ) . '</p></div>';
        endif;
        wp_die();
    }

}
add_action( 'wp_ajax_mst_display_breakdown_item', 'mst_display_breakdown_item' );
add_action( 'wp_ajax_nopriv_mst_display_breakdown_item', 'mst_display_breakdown_item' );