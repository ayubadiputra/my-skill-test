<?php

/**
 * Single custom post type Test display
 *
 * @package My_Skill_Test
 * @since My_Skill_Test 1.0.0
 */

get_header( 'mst_test' );

while ( have_posts() ) : the_post(); ?>

<div <?php mst_generated_class(); ?>>
    <div class="row">
        <div class="col-md-12">
            <div class="mst-summary">
                <h1 class="mst-title">
                    <?php the_title(); ?>
                </h1>
                <div class="mst-meta">
                    <span class="mst-meta-item mst-user">
                        <label>Completed by:</label> <?php echo get_the_author(); ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-l-6 col-xs-12">
            <?php do_action( 'mst_display_results' ); ?>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-l-6 col-xs-12">
            <?php do_shortcode( '[mst-breakdown mst-id="' . get_the_ID() . '"]' ); ?>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php do_action( 'mst_display_history' ); ?>
        </div>
    </div>
</div>

<?php endwhile; ?>

<?php get_footer( 'mst_test' ); ?>