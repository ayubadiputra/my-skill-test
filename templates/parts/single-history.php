<?php

/**
 * Single custom post type Skill Test history display.
 *
 * @package My_Skill_Test
 * @since My_Skill_Test 1.0.0
 */

?>

<div class="history single-section">
    <h4 class="title"><?php _e( 'History', MST_DOMAIN ); ?></h4>
    <div class="history-area">
        <ul class="history-list">
            <li>
                <?php _e( 'Invitation email on' ); ?> <?php echo $mst_history['invitation']; ?>
            </li>
            <li>
                <?php _e( 'Candidate started the test on' ); ?> <?php echo $mst_history['started']; ?>
            </li>
            <li>
                <?php _e( 'Candidate completed the test on' ); ?> <?php echo $mst_history['completed']; ?>
            </li>
        </ul>
    </div>
</div>