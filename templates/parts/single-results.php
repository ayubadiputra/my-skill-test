<?php

/**
 * Single custom post type Skill Test result display.
 *
 * @package My_Skill_Test
 * @since My_Skill_Test 1.0.0
 */

?>

<div class="results single-section">
<h4 class="title"><?php _e( 'Result', MST_DOMAIN ); ?></h4>
    <div class="row">
        <div class="col-xs-6 col-xs-l-offset-2 col-sm-offset-0 col-sm-6 col-md-offset-0 col-md-5">
            <div class="mst-chart-area">
                <div class="ct-chart ct-square ct-container">
                    <div id="ct-total" class="ct-total" data-total="<?php echo $mst_result['value']; ?>">
                        <?php _e( 'Total', MST_DOMAIN ); ?>
                    </div>
                    <div id="ct-separator" class="ct-separator"></div>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-6 col-xs-l-limit-2">
            <div class="mst-notes">
                <div class="mst-notes-item">
                    <label><?php _e( 'Status', MST_DOMAIN ); ?>:</label>
                    <div class="mst-notes-content">
                        <span class="label label-warning">
                            <?php echo esc_html( $mst_result['status'] ); ?>
                        </span>
                        <?php mst_change_link(); ?>
                    </div>
                </div>
                <div class="mst-notes-item">
                    <label>Time:</label>
                    <div class="mst-notes-content">
                        <p class="time">
                            <?php echo esc_html( $mst_result['time_taken'] ); ?>
                        </p>
                        <p>(of max <?php echo esc_html( $mst_result['max_time'] ); ?>)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

var cttotal = document.getElementById( 'ct-total' );
var ctsepr  = document.getElementById( 'ct-separator' );

var total = parseInt( cttotal.getAttribute( 'data-total' ) );
var diff  = 100 - total;

var chart = new Chartist.Pie('.ct-chart', {
    series: [{
        name: 'done',
        className: 'ct-done',
        value: total
    }, {
        name: 'outstanding',
        className: 'ct-outstanding',
        value: diff
    }]
}, {
    donut: true,
    chartPadding: 0,
    labelInterpolationFnc: function(value) {
        var total = chart.data.series.reduce(function(prev, series) {
            return prev + series.value;
        }, 0);
        return Math.round(value / total * 100) + '%';
    }
});

chart.on( 'draw', function(ctx) {
    if ( ctx.type === 'label' ) {

        // Adjust margin between total and precentage
        var margin_both = [8, 34, 25];

        if( ctx.index === 0 ) {
            ctx.element.attr({
                dx: ctx.element.root().width() / 2,
                dy: ( ctx.element.root().height() / 2 ) + margin_both[0]
            });

            // Set y position
            var y_pos = ( ctx.element.root().height() / 2 ) - margin_both[1];
            cttotal.style.top = y_pos + 'px';

            y_pos = ( ctx.element.root().height() / 2 ) + margin_both[2];
            ctsepr.style.top  = y_pos + 'px';

            // Get precentage width
            var w_label = ctx.element.width();
            ctsepr.style.width = w_label + 'px';

        } else {
            ctx.element.remove();
        }
    }
});
</script>