<?php

/**
 * Single custom post type Skill Test breakdown display.
 *
 * @package My_Skill_Test
 * @since My_Skill_Test 1.0.0
 */

?>

<div class="mst-breakdown single-section">
    <h4 class="title"><?php _e( 'Breakdown', MST_DOMAIN ); ?></h4>
    <div id="mst-breakdown-single" data-id="<?php echo esc_attr( $mst_breakdown['id'] ); ?>" class="mst-breakdown-area"><?php _e( 'Waiting for load ...', MST_DOMAIN ); ?></div>
</div>