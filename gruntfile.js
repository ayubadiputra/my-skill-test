module.exports = function(grunt) {
    grunt.initConfig({
        sass: {
            // this is the "dev" Sass config used with "grunt watch" command
            dev: {
                options: {
                    style: 'expanded',
                    // tell Sass to look in the Bootstrap stylesheets directory when compiling
                    loadPath: ['node_modules/bootstrap-sass/assets/stylesheets', 'bower_components/chartist/dist/scss']
                },
                files: {
                    // the first path is the output and the second is the input
                    'assets/css/my-skill-test.css': 'assets/scss/my-skill-test.scss',
                    'assets/css/my-skill-test-admin.css': 'assets/scss/my-skill-test-admin.scss',
                    'assets/third-party/chartist/chartist.css': 'assets/third-party/chartist/scss/mst-chartist.scss'
                }
            },
            // this is the "production" Sass config used with the "grunt buildcss" command
            dist: {
                options: {
                    style: 'compressed',
                    loadPath: ['node_modules/bootstrap-sass/assets/stylesheets', 'bower_components/chartist/dist/scss']
                },
                files: {
                    'assets/css/my-skill-test.css': 'assets/scss/my-skill-test.scss',
                    'assets/css/my-skill-test-admin.css': 'assets/scss/my-skill-test-admin.scss',
                    'assets/third-party/chartist/chartist.css': 'assets/third-party/chartist/scss/mst-chartist.scss'
                }
            }
        },
        // configure the "grunt watch" task
        watch: {
            scripts: {
                files: 'assets/*.scss',
                tasks: ['sass:dist'],
                options: {
                    debounceDelay: 250,
                },
            },
        },
        // configure the "grunt copy" task
        copy: {
            main: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['bower_components/chartist/dist/*.js', 'bower_components/chartist/dist/*.js.map'],
                    dest: 'assets/third-party/chartist/',
                    filter: 'isFile'
                }],
            },
        },
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // "grunt buildcss" is the same as running "grunt sass:dist".
    // if I had other tasks, I could add them to this array.
    grunt.registerTask( 'default', ['sass:dist']);
};