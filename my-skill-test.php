<?php

/*
Plugin Name: My Skill Test
Plugin URI: https://github.com/ayubadiputra/my-skill-test
Description: Set up custom skill test post type and taxonomies with additional custom meta field.
Version: 1.0.0
Author: ayubadiputra
Author URI: https://github.com/ayubadiputra
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: my-skill-test
Domain Path: /languages

My Skill Test is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

My Skill Test is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with My Skill Test. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/

// Prevent from direct access
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Handle plugin not shows any data directly
if ( ! function_exists( 'add_action' ) ) {
    echo __( "Hi, please don't access me directly. Thanks!", 'my-skill-test' );
    exit;
}

if ( ! class_exists( 'My_Skill_Test' ) ) :

/**
 * My_Skill_Test main class.
 *
 * Set constants, load all required files, register activation, deactivation, uninstall
 * hook, register new post type and taxonomy.
 *
 * @class My_Skill_Test
 * @package My_Skill_Test
 * @since 1.0.0
 */
final class My_Skill_Test {

    /**
     * Single instance of My_Skill_Test class
     *
     * @var null
     */
    protected static $instance = null;

    /**
     * Ensure only one instance of My_Skill_Test class in entire process
     *
     * @return object Instance
     */
    public static function init() {

        // Check if instance is exist
        if ( null === static::$instance ) {
            static::$instance = new static;
        }

        return static::$instance;

    }

    /**
     * Prevent cloning of the instance
     */
    private function __clone() {}

    /**
     * Prevent unserializing of the instance
     */
    private function __wakeup() {}

    /**
     * My_Skill_Test constructor to run the actions
     */
    public function __construct() {
        $this->set_constant();
        $this->require_files();
        $this->activation();
    }

    /**
     * Set constants of My_Skill_Test
     */
    private function set_constant() {
        // Set all constant
        $this->define( 'MST_DOMAIN',           'my-skill-test' );
        $this->define( 'MST_MAIN_FILE',        __FILE__ );
        $this->define( 'MST_DIR',              trailingslashit( plugin_dir_path( __FILE__ ) ) );
        $this->define( 'MST_DIR_INC',          trailingslashit( MST_DIR . 'inc' ) );
        $this->define( 'MST_DIR_TEMPLATES',    trailingslashit( MST_DIR . 'templates' ) );
        $this->define( 'MST_URI',              trailingslashit( plugin_dir_url( __FILE__ ) ) );
        $this->define( 'MST_URI_ASSETS',       trailingslashit( MST_URI . 'assets' ) );

        // Admin only
        if ( is_admin() ) {
            $this->define( 'MST_DIR_ADMIN',    trailingslashit( MST_DIR . 'inc/admin' ) );
        }
    }

    /**
     * Check the constant is exist, set the constant if not defined yet then
     *
     * @param  string $constant Constant name
     * @param  mixed  $value    Constant value
     * @return                  Return if $constant or $value is null
     */
    private function define( $constant = null, $value = null ) {

        // Check parameter is not null
        if ( $constant == null || $value == null ) {
            return;
        }

        // Check constant is defined or not yet
        if ( ! defined( $constant ) ) {
            define( $constant, $value );
        }

    }

    /**
     * Include all required files
     */
    private function require_files() {

        // Includes all required files
        require_once MST_DIR_INC . 'mst-helper-functions.php';
        require_once MST_DIR_INC . 'mst-action-functions.php';
        require_once MST_DIR_INC . 'class-mst-activation.php';
        require_once MST_DIR_INC . 'class-mst-post-type.php';

        // Admin only
        if ( is_admin() ) {
            // require_once MST_DIR_ADMIN . 'class-mst-admin-controller.php';
            require_once MST_DIR_ADMIN . 'core/class-mst-admin-view.php';
            require_once MST_DIR_ADMIN . 'class-mst-meta-boxes.php';
            require_once MST_DIR_ADMIN . 'class-mst-admin-assets.php';
        } else {
            require_once MST_DIR_INC . 'mst-templates-functions.php';
            require_once MST_DIR_INC . 'class-mst-assets.php';
        }
    }

    /**
     * Register activation, deactivation, and uninstall hooks
     */
    private function activation() {
        // Activation and deactivation hooks
        register_activation_hook(   __FILE__, array( 'MST_Activation', 'activation' ) );
        register_deactivation_hook( __FILE__, array( 'MST_Activation', 'deactivation' ) );
        register_uninstall_hook(    __FILE__, array( 'MST_Activation', 'uninstall' ) );
    }

}

endif;

/**
 * Enable to use instance in entire process without create new Global variables
 *
 * @package My_Skill_Test
 * @since 1.0.0
 *
 * @return object My_Skill_Test instance
 */
function My_Skill_Test() {
    return My_Skill_Test::init();
}

// Run main class
My_Skill_Test();