/* Wait until breakdown section loaded */
jQuery( '#mst-breakdown-single' ).ready( function () {

    $this = jQuery( this );

    /* Set ajax request action */
    var data = {
        'action': 'mst_display_breakdown_item',
        'mst_id': jQuery( '#mst-breakdown-single' ).data( 'id' )
    };

    jQuery.post( mst_ajax.ajax_url, data, function( response ) {
        jQuery( '#mst-breakdown-single' ).empty().append( response );
    });

} );