/* Wait untul meta box ready */
jQuery( '#mst-skill-test-meta' ).ready( function() {

    /* Load datetimepicker */
    jQuery( '#test_invitation' ).datetimepicker();
    jQuery( '#test_start' ).datetimepicker();
    jQuery( '#test_complete' ).datetimepicker();

    /* Handle add new row for skill taken */
    jQuery( '.mst-skill-taken-table' ).on( 'click', '#add-new-row', function() {
        $table = jQuery( '.mst-skill-taken-table' ).find( 'tbody' );
        $row   = $table.find( 'tr:first' ).clone();
        $table.find( 'tr:last' ).prev().after( $row );
        $table.find( 'tr:last' ).prev().find( 'input' ).val( '' );
    });

    /* Handle delete row for skill taken */
    jQuery( '.mst-skill-taken-table' ).on( 'click', '.remove-row', function() {
        if ( jQuery( '.mst-skill-taken-table' ).find( '.skill_data' ).length > 1 ) {
            $row = jQuery( this ).parents( '.skill_data' ).remove();
        }
    });

});